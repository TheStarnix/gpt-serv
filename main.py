import json

import requests
from flask import Flask, request, jsonify

import prompt_validation
import prompt_generate

app = Flask(__name__) #Lorsqu'on voudra appeler Flask, on utilisera app


@app.route("/generate", methods=['POST'])
def generate():
    #Get results
    data = request.get_json()

    #Get the prompt
    prompt = data["prompt"]

    #Get the history
    history = data["history"]
    if prompt_validation.check_prompt(prompt) == False:
        return jsonify({"tag": "Unsuccessful"})
    else:
        result,history,prompt = prompt_generate.prompt_generate(prompt, history)
        print("== HISTORY ==")
        print(history)
        print(type(history))
        print("== PROMPT ==")
        print(prompt)
        print(type(prompt))
        print("== RESULT ==")
        print(result)
        print(type(result))
        resultJsonify = jsonify({"tag": "Success", "result": {"history": history, "prompt": prompt, "response": result}})
        return resultJsonify

#Lancer l'application via le terminal
#Pour lancer l'appli: python main.py
if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5000)
