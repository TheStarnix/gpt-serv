import os

from langchain import PromptTemplate, LLMChain
from langchain.llms import GPT4All
from gpt4all import gpt4all
from langchain.memory import ConversationBufferMemory


def prompt_generate(request, history):
    template = """\n### Instructions:\nAs an assistant, your goal is to answer the questions of the user. The assistant have access to the history conversation to remember informations. The assistant can understand and communicate fluently in the user's language of choice such as English, 中文, 日本語, Español, Français or Deutsch.\n### Conversation :\n{history}\nUser: {prompt}\nAssistant:"""
    gpt = gpt4all.GPT4All(model_name="nous-hermes-13b.ggmlv3.q4_0.bin", model_path=os.getcwd()+"/models/", allow_download=True)
    gpt=GPT4All(model="models/nous-hermes-13b.ggmlv3.q4_0.bin", echo=True,streaming=True, temp=0.28, top_p=0.95, n_predict=200, repeat_penalty=1.2,n_threads=4)

    response = ""
    print("== CHAT ==")
    prompt = PromptTemplate(input_variables=["prompt", "history"], template=template)
    gpt_chain = LLMChain(llm=gpt, prompt=prompt, verbose=True)
    response = gpt_chain.predict(prompt=request, history=history)
    response = str(response)
    print("== RESPONSE ==")
    print(response)

    return response, history, request